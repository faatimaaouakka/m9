import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class Caja implements Runnable{

	private Cliente cliente;
	private long initialTime;

	public Caja(Cliente cliente) {

		this.cliente = cliente;
	}

	// Constructores getters y setters
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public long getInitialTime() {
		return initialTime;
	}
	public void setInitialTime(long initialTime) {
		this.initialTime = initialTime;
	}

	@Override
	public void run() {
		
		System.out.println( 
					"--Creant el " + 
					cliente.getNombre() + " amb " + cliente.getCarroCompra().length + " articles " + Arrays.toString(cliente.getCarroCompra()) + "--");
		System.out.println("  " + cliente.getNombre()+ " passa per caixa..");
		
		for (int i = 0; i < cliente.getCarroCompra().length; i++) {
			long init = System.currentTimeMillis();
			// Esperamos X segundos para processar el siguiente articulo 
				// Num random entre 2 y 8
			esperarXsegundos(cliente.getCarroCompra()[i]);

			// Si no es el ultimo articulo del carro mostramos un mensaje
				// Y si lo es mostramos otro
			if (i == cliente.getCarroCompra().length-1) {
				System.out.println( cliente.getNombre() + " - article " + (i + 1) + "/" + cliente.getCarroCompra().length +
						" -> Tiempo: " + (System.currentTimeMillis() - init)/ 1000 +
						"seg..." + " FINALITZAT");
			}else {
				System.out.println( cliente.getNombre() + " - article " + (i + 1) + "/" + cliente.getCarroCompra().length +
						" -> Tiempo: " + (System.currentTimeMillis() - init)/ 1000 +
						"seg");
			}
			
		}

	}
 
	private static void esperarXsegundos(int segundos) {
		try {
			//segundos a esperar 
			Thread.sleep(segundos * 1000);
		} catch (InterruptedException ex) {
			Thread.currentThread().interrupt();
		}
	}
	
	public static void main(String[] args){
		int numCajas = 10; // Numero de hilos

		// Array que almacena los objetos de clientes 
		ArrayList<Cliente> clientes = new ArrayList<Cliente>();

		// Creamos 10 clientes 
		for (int i = 0; i < 50; i++) {  
			// Numero random de productos de 1 a 30, (posiciones del array de productos)
			int[] numeros = new int[(int)(Math.random()*31)+1];
			
			// Rellenamos el array de productos con numeros aleatorios entre 2 y 8
			for (int j = 0; j < numeros.length; j++) {
				numeros[j] = (int)(Math.random() * 7)+2;
				
			}
			// A�adimos el cliente en el arrayList creado anteriormente
			clientes.add(new Cliente("Client "+(i+1) , numeros));

		}  
			

		// Fijamos el numero de threads a ejecutar 
		ExecutorService executor = Executors.newFixedThreadPool(numCajas);
		
		// recorremos el arrayList y y decimos que a cada Cliente le procese el pedido una caja
		for (Cliente cliente: clientes) {
			// Espera 3 segundos antes de crear otro cliente 
			esperarXsegundos(3);
			Runnable caja = new Caja(cliente);
			executor.execute(caja);
	        
		}
		executor.shutdown();

        		
	}

} 

