import java.util.concurrent.ForkJoinPool; 
import java.util.concurrent.RecursiveTask;

public class Apartat3  extends RecursiveTask<Double>{

	double a;
	double b;

	public Apartat3(double a, double b){
		this.a = a;
		this.b = b;
	}    

	public static double power(double x, double n) {
		if (n == 0) {
			return 1.0;
		}
		else if (n > 0) {
			return x * power(x, n - 1);
		}
		else {
			return 1.0 / x * power(1.0 / x, -n - 1); 
		}
	}

	@Override
	protected Double compute() {

		// ATENCIO **1** 
		double calcul = power(a, b);
		
		if(b <= 1) return b;		

		Apartat3 fib2 = new Apartat3(a, b-1);
		fib2.fork();

		return  power(a, b) + fib2.compute();
	}



	public static void main(String[] args){
		double numA = 16;
		double numB = 40;

		double startFil = System.currentTimeMillis();

		ForkJoinPool pool = new ForkJoinPool();

		System.out.println("Calcul fils:  " + pool.invoke(new Apartat3(numA, numB)));

		double stopFil = System.currentTimeMillis();

		System.out.println((stopFil- startFil) / 1000);



		double start = System.currentTimeMillis();

		System.out.println("Calcul:  " + power(numA, numB));

		double stop = System.currentTimeMillis();

		System.out.println((stop - start) / 1000);
	}


}
