class TiradaDaus {
	private int tiradaDau;

	public TiradaDaus (int e) {
		tiradaDau=e;
	}

	public int getSumaTirada() {
		return tiradaDau;
	}

	public void setSumaTirada(int e) {
		tiradaDau += e;
	}
}

